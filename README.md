# Meus experimentos com ESP01

Para liberar a porta para gravação do ESP:

```bash
ls -l /dev/ttyUSB*
```

A resposta deve ser algo parecido:

```bash
crw-rw---- 1 root dialout 188, 1 abr  3 16:33 /dev/ttyUSB1
```

O nome logo depois do root é o grupo no qual precisamos colocar o usuário do sistema.

```bash
sudo usermed -a -G dialout <username>
```

Encerre a sessão e faça login novamente (Não é necessário reiniciar).

OBS:

Em alguns sketches é necessário criar um arquivo `credenciais.h`, esse arquivo leva as credenciais de conexão com a rede ou outro app quando necessário,  
exemplo:

```c++
const char* SSID = "SSID da rede";
const char* PASSWORD = "Senha da rede";
const char* TOKEN = "token utilizado no app para conexão"; // Utilizado pelo Blynk por exemplo
```

Esse arquivo deve estar na mesma pasta que o sketch que o utiliza está.
