#include <ESP8266WiFi.h>
#include "credenciais.h"

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println("Preparando\n");

  WiFi.begin(SSID, PASSWORD);
  Serial.print("Connecting to: ");
  Serial.print(SSID); Serial.print("...");
  
  int i = 0;
  while (WiFi.status() != WL_CONNECTED){
    Serial.println(WiFi.status());
    delay(500);
    Serial.print(++i); Serial.print(".");
  }

  Serial.print("\n");
  Serial.println("Connection established!");
  Serial.print("IP address:\t");
  Serial.println(WiFi.localIP());

}

void loop() {
  // put your main code here, to run repeatedly:
  delay(1000);
  Serial.println("Connection established!");
  Serial.print("IP address:\t");
  Serial.println(WiFi.localIP());

}
