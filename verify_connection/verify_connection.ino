#include <ESP8266WiFi.h>

void setup() {
  Serial.begin(115200);
  Serial.println('Preparando o Setup');
  
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.print("Scan start...");
  int n = WiFi.scanNetworks();
  Serial.print(n);
  Serial.println(" Networks found");
  for (int i = 0; i < n; i++) {
    Serial.println(WiFi.SSID(i));
  }
  Serial.println();
  delay(5000);
}
